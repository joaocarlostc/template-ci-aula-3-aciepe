# Pré-requisitos

## Seleção do Recipe

Adicione o caminho do _singularity recipe_ desejado na variável RECIPE no gitlab (projeto >> settings >> CI/CD).

# Template Integração Contínua GitLab

Esse projeto o template para uso do cluster da UFSCar, contendo integração contínua com o Google Drive e Amazon S3

## Requisitos para Google Drive

1. Entre no [console de credenciais de API do Google](https://console.developers.google.com/apis/credentials)
2. Se ainda não houver um projeto, crie um com permissão para a "Google Drive API".
3. Clique em "Criar credenciais".
4. Selecione "ID do cliente do OAuth".
5. Em "Tipo de aplicativo", selecione "App para computador".
6. Dê um nome de identificação para as credenciais e clique em "criar". Vão aparecer dois dados ("Seu ID de cliente" e "Sua chave secreta de cliente"), precisaremos dos dois no passo seguinte.
7. Acesse o _cluster_, execute o comando `rclone config` e forneça as seguintes informações quando solicitado:

```
n/s/q> n
name> cloud
Storage> 13
client_id> conteúdo de "Seu ID de cliente"
client_secret> conteúdo de "Sua chave secreta de cliente"
scope> 1
root_folder_id> deixe em branco
service_account_file> deixe em branco
y/n> deixe em branco
y/n> n

Copie o url e cole no navegador no computador local. Autorize e:

Enter verification code> código fornecido pelo navegador após autorização
y/n> deixe em branco
y/e/d> deixe em branco
e/n/d/r/c/s/q> q
```

A configuração até agora servirá para transferência de dados do _cluster_ para a nuvem e vice-e-versa. A partir de agora vamos configurar a integração contínua no gitlab.

8. Execute o comando:

```
echo $(cat ~/.config/rclone/rclone.conf | base64 --wrap=0)
```

9. Copie a saída e adicione à variável RCLONE_CONF no gitlab.
10. Adicione no gitlab à variável COLLECTION_CONTAINER `/path/to/project`, esse será o caminho em que container vai ser disponibilizado no seu Google Drive no formato `recipe_name_DateTime.simg`.

## Requisitos para Amazon S3

1. Entre na [AWS](https://console.aws.amazon.com/)
2. Clique na seta ao lado de seu nome de usuário e em "My Security Credentials".
3. Na seção "Access Keys", clique em "Create New Access Key".
4. Na janela que aparece, clique em "Show Access Key".
5. Acesse o _cluster_, execute o comando `rclone config` e forneça as seguintes informações quando solicitado:

```
n/s/q> n
name> cloud
Storage> 4
provider> 1
env_auth> 1
access_key_id> conteúdo de "Access Key ID"
secret_access_key> conteúdo de "Secret Access Key"
region> 16
endpoint> deixe em branco
location_constraint> 16
acl> deixe em branco
server_side_encryption> deixe em branco
sse_kms_key_id> deixe em branco
storage_class> deixe em branco
y/n> deixe em branco
y/e/d> deixe em branco
e/n/d/r/c/s/q> q
```

A configuração até agora servirá para transferência de dados do _cluster_ para a nuvem e vice-e-versa. A partir de agora vamos configurar a integração contínua no gitlab.

6. Execute o comando:

```
echo $(cat ~/.config/rclone/rclone.conf | base64 --wrap=0)
```

7. Copie a saída e adicione à variável RCLONE_CONF no gitlab.
8. Adicione no gitlab à variável COLLECTION_CONTAINER `/path/to/project`, esse será o caminho em que container vai ser disponibilizado no seu Google Drive no formato `recipe_name_DateTime.simg`.

# MANUAL DE EXECUÇÃO DO CÓDIGO NO CLUSTER

## Código fonte em C

1. O código fonte utilizado para o projeto final é o _multiplica.c_, que se econtra no diretório `SourceExample/multiplica.c`. Esse código realiza a multiplicação de matrizes de forma paralela, utilizando o MPI e o OpenMP.

2. Os parâmetros de dimensão da matriz se encontram no diretório `Input/parametros.txt`, onde são escritos sequêncialmente para: Linhas A, Colunas A / Linhas B, Colunas B.

3. A pasta Input necessita estar sincronizada com a pasta Input do Sistema de Armazenamento de dados remoto, no meu caso o Google Drive. Para fazer a sincronização, é necessário ir ao arquivo `.gitlabci/send.sh` e alterar a ultima linha, que possui o rclone sync, para o path de input do seu Google Drive.

## Criação do container

1. Crie um repositório local em seu computador ou apenas utilize o editor próprio do  GitLab

2. O arquivo de Recipe do container se encontra em `Recipes/Singularity_example`. Abra ele

3. Mudando a sua Recipe do modo que quiser (é necessário alguma mudança para realizar o commit). Se estiver no repositório local, de um commit e um push. Se estiver editando no próprio GitLab, apenas um commit é suficiente

4. Com isso, o processo CI/CD começa a ser executado para a criação do container utilizado no cluster, no qual irá ser salvo no diretório indicado no Google Drive

## Execução do JOB

1. Com o container criado, acesse o Cluster UFSCar

2. Copie o código presente em `Jobs/job_mpi1.sh` no repositório para dentro do Cluster

3. Nesse código, estão comentadas todas as configurações para serem alteradas nas suas configurações: As do Google Drive, dos Bots do Discord e Telegram, das pastas criadas e a execução do container. Altere-as para o seu funcionamento!

4. Antes de qualquer coisa, apague no código mpirun a flag hostfile e o arquivo hosts.txt e submeta o job com o comando

```
sbatch job_mpi1.sh
```

   E note na fila quais nós estão sendo utilizados para a execução do código.
   
5. A partir dessa informação, coloque no arquivo `Input/hosts.txt` os nós utilizados pelo cluster e seus slots distribuidos de forma adequada. Isso foi notado com testes executados no cluster, sendo necessário isso para poder executar com o arquivo hostfile. Existe um método sem a utilização dele, utilizando o comando srun, mas isso acabou não sendo testado.

6. Adicione novamente a flag _-hostfile_ e o endereço `Input/hosts.txt` com as alterações feitas acima.

7. Feito isso, basta executar utilizando o comando do passo 4! 

6. Após o termino de execução, irão ser criados na pasta do seu cluster, os arquivos _output.txt_ e _error.txt_, que mostram respectivamente a saída padrão e a de erro. Nos bots, serão enviados esses arquivos também com algumas mensagens guias.
