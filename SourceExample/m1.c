#include "mpi.h"
#include <stdio.h>

int main( int argc, char *argv[]) {
	// Variaveis para o programa
	int numtasks, rank, status, namelen;
	
	// Variavel para adquirir o nome do processador
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	
	// Inicia o MPI
	status = MPI_Init(&argc,&argv);

	if (status != MPI_SUCCESS) {
		printf ("Erro em MPI_Init. Terminando...\n");
		MPI_Abort(MPI_COMM_WORLD, status);
	}
	
	// Para cada processo, é adquirido o rank dele e a quantidade total de processos
	MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	// int MPI_Get_processor_name(char *name, int *resultlen);
	// Returns the name of the processor on which it was called.
	MPI_Get_processor_name(processor_name, &namelen);

	// Imprime para cada processo os dados
	printf("Processo %d de %d em %s\n", rank,numtasks,processor_name);
	
	// Finaliza o MPI
	MPI_Finalize();

	return(0);
}
