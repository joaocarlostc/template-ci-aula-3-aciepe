#!/bin/bash

# CABECALHO ===========================================

#SBATCH -J job_teste                              # Nome do Job                   
#SBATCH --output=output.txt                       # Arquivo de saída padrão
#SBATCH --error=error.txt                         # Arquivo de saída de erro
#SBATCH -n 280                                    # Numero de processos
#SBATCH -N 7                                      # Numero de nós
#SBATCH -t 01:00:00                               # Tempo limite de execução
#SBATCH --mem=10000                               # Memoria para utilizar (MB)
#SBATCH --mail-user=campijoao@estudante.ufscar.br # Email para envio dos resultados
#SBATCH --mail-type=ALL                           # Informações enviadas por email
#SBATCH --account=u789053                         # Conta responsavel pelo job

# CORPO ==============================================

# Nome do perfil do rclone - Mudar para o seu todas essas variaveis do drive a partir da raiz
service="cloud"

# Pasta de input no drive
remote_in="hpc/containers/Input"

# Pasta de output no drive
remote_out="hpc/containers/Output"

# Pasta com o container
remote_sing="hpc/containers/aula_3"

# Pasta de input no container
container_in="/opt/input" # O programa deve ler dessa pasta

# Pasta de output no container
container_out="/opt/output" # O programa deve escrever nessa pasta

# Pasta onde o container sera salvo no cluster
local_sing="." # Local onde o job foi executado

#  Pasta temporaria no cluster
local_job="/scratch/job.${SLURM_JOB_ID}"

# Pasta de entrada no cluster
local_in="${local_job}/input"

# Pasta de saida no cluster
local_out="${local_job}/output"

# TRAP ==================================================

function limpar(){
	echo "Limpando a pasta local_job..."
	rm -rf "${local_job}"
}
trap limpar EXIT HUP INT TERM ERR # Define a funcao limpar como uma trap

set -eE # Para a trap funcionar quando der erro

# FUNCAO DE ERRO ========================================
function sendErr(){ # Envia uma mensagem de erro, a saida de erro e a saida padrao para o telegram
  echo "Erro!!!!"
  sendMsgDiscord "Erro no Job_${SLURM_JOB_ID}!"
  sendMsgTelegran "Erro no Job_${SLURM_JOB_ID}"
  sendFileDiscord "error.txt"
  sendFileTelegran "error.txt"
  sendFileDiscord "output.txt"
  sendFileTelegran "output.txt"
  cleanJob
}
trap sendErr ERR

# CONFIGURACAO DAS PERMISSOES ===========================

umask 077 # Remove as permissoes de escrita, leitura e execucao dos outros usuarios

# Define o arquivo a ser copiado como o mais recente na pasta especificada
sing=$(rclone lsf --max-depth 0 "${service}:${remote_sing}/" --files-only --format "tp" | grep simg | grep example | sort | tail -1)
sing=${sing:20}

echo "Copiando container..."
rclone copyto "${service}:${remote_sing}/${sing}" "${local_sing}/Singularity.simg"

# CRIACAO DE PASTAS TEMPORARIAS =========================

echo "Criando pastas temporarias..."

mkdir -p "${local_in}"
mkdir -p "${local_out}"

# COPIA DA INPUT ========================================

echo "Copiando input..."

rclone copy "${service}:${remote_in}/" "${local_in}" # Copia os arquivos na pasta de entrada do drive
 
echo $(ls ${local_in})

# FUNCOES BOT ===========================================

echo "Definindo Variaveis dos Bots"

# Variaveis telegram =============================================
bot_key="bot1480514664:AAHvSYRKV1T2GezzQ5kAcEyER4WCuS0BIx0" # bot
chat_id="807196515" # chat onde o bot ira enviar as mensagens

# Variaveis discord ======================================================================================================================================
discord_webhook="https://discord.com/api/webhooks/782781617055465482/ZlsjIXKbQx3uus_4pzkcCwtX9istXJreBAEYf1bIyOGWwlH9zD4e2gh7rcXS2mGLpj6O" # webhook obtido

echo "Definindo Funcoes dos Bots"

# Funcoes Telegram =======================================================================================

echo "Telegram"

function sendPhotoTelegram(){
 #curl https://api.telegram.org/$bot_key/sendphoto -F "chat_id=$chat_id" -F "photo=@$1"
  curl -F "photo=@$1" https://api.telegram.org/$bot_key/sendphoto?chat_id=$chat_id
}

function sendFileTelegram(){
  curl -F "document=@$1" https://api.telegram.org/$bot_key/sendDocument?chat_id=$chat_id
}

function sendMsgTelegram(){
  curl -X POST -H 'Content-Type: application/json' -d "{\"chat_id\": \"$chat_id\", \"text\": \"$1\", \"disable_notification\": true}"  "https://api.telegram.org/$bot_key/sendMessage"
}

echo "Discord"

# Funcoes Discord =========================================================================================
function sendFileDiscord(){
        curl -i -H 'Expect: application/json' -F file=@$1 -F 'payload_json={ "wait": true}' $discord_webhook
}

function sendMsgDiscord(){
	curl -X POST -H "Content-Type: application/json" -d "{\"content\": \"$1\"}" $discord_webhook 
}


# EXECUCAO DO PROGRAMA ===================================

echo "Executando job_${SLURM_JOB_ID}..."

sendMsgDiscord "Executando job_${SLURM_JOB_ID}..."
sendMsgTelegram "Executando job_${SLURM_JOB_ID}..."

# Mudar o srun da maneira que precisar. sing - Container. Após dele é o programa e seus parametros

echo "$SLURMD_NODENAME"

echo $(cat ${local_in}/hosts.txt)

mpirun -np 140 -hostfile ${local_in}/hosts.txt singularity exec \
	--bind=/scratch:/scratch \
	--bind=/var/spool/slurm:/var/spool/slurm \
	--bind="${local_in}:${container_in}" \
	--bind="${local_out}:${container_out}" \
	Singularity.simg /opt/multiplica $(cat ${local_in}/parametros.txt)
     
# ENVIO DOS ARQUIVOS DE SAIDA ============================
# criar um novo job para isso caso tenham muitos arquivos

echo "Enviando arquivos de saida..."

rclone move "${local_out}" "${service}:${remote_out}" # Envio dos arquivos na pasta de saida do cluster
rclone copy "output.txt" "${service}:${remote_out}" # Envio do arquivo definido como saida padrao do programa

sendMsgDiscord "Finalizado job_${SLURM_JOB_ID}!"
sendFileDiscord "output.txt"

sendMsgTelegram "Finalizado job_${SLURM_JOB_ID}!"
sendFileTelegram "output.txt"

